#! /bin/bash
set -e
trap 'previous_command=$this_command; this_command=$BASH_COMMAND' DEBUG
trap 'echo FAILED COMMAND: $previous_command' EXIT

#-------------------------------------------------------------------------------------------
# This script will download packages for, configure, build and install a GCC cross-compiler.
# Customize the variables (INSTALL_PATH, TARGET, etc.) to your liking before running.
# If you get an error and need to resume the script from some point in the middle,
# just delete/comment the preceding lines before running it again.
#
# See: http://preshing.com/20141119/how-to-build-a-gcc-cross-compiler
#-------------------------------------------------------------------------------------------

INSTALL_PATH=$(git rev-parse --show-toplevel)/xtools
BUILD_PATH=${INSTALL_PATH}/build

TARGET=riscv64-unknown-linux-gnu
ABI="lp64"
ISA="rv64g"
SYSROOT=${INSTALL_PATH}/${TARGET}

USE_NEWLIB=0
LINUX_ARCH=riscv

PARALLEL_MAKE=-j4

BINUTILS_VERSION=binutils-2.32
GCC_VERSION=gcc-9.2.0
LINUX_KERNEL_VERSION=linux-5.2.18
GLIBC_VERSION=glibc-2.29
QEMU_VERSION=qemu-4.1.0

export PATH=$INSTALL_PATH/bin:$PATH

pushd .

mkdir -p ${BUILD_PATH}
cd ${BUILD_PATH}

# Download packages
export http_proxy=$HTTP_PROXY https_proxy=$HTTP_PROXY ftp_proxy=$HTTP_PROXY
wget -nc -P ${BUILD_PATH} https://ftp.gnu.org/gnu/binutils/$BINUTILS_VERSION.tar.gz
wget -nc -P ${BUILD_PATH} https://ftp.gnu.org/gnu/gcc/$GCC_VERSION/$GCC_VERSION.tar.gz
if [ $USE_NEWLIB -ne 0 ]; then
    curl -o ${BUILD_PATH}/newlib-master.zip https://github.com/bminor/newlib/archive/master.zip || true
    unzip -qo newlib-master.zip
else
    wget -nc -P ${BUILD_PATH} https://www.kernel.org/pub/linux/kernel/v5.x/$LINUX_KERNEL_VERSION.tar.xz
    wget -nc -P ${BUILD_PATH} https://ftp.gnu.org/gnu/glibc/$GLIBC_VERSION.tar.xz
fi
wget -nc -P ${BUILD_PATH} https://download.qemu.org/$QEMU_VERSION.tar.xz

# Extract everything
for f in *.tar*; do tar xf $f -C ${BUILD_PATH} ; done

# Make symbolic links
# cd $GCC_VERSION
# ln -sf `ls -1d ../mpfr-*/` mpfr
# ln -sf `ls -1d ../gmp-*/` gmp
# ln -sf `ls -1d ../mpc-*/` mpc
# ln -sf `ls -1d ../isl-*/` isl
# ln -sf `ls -1d ../cloog-*/` cloog
# cd ..

# Step 1. Binutils
mkdir -p build-binutils
cd build-binutils
../$BINUTILS_VERSION/configure --prefix=$INSTALL_PATH --target=$TARGET --disable-multilib --disable-werror --disable-nls --with-expat=yes #--enable-gdb
make $PARALLEL_MAKE
make install
cd ..

# Step 2. Linux Kernel Headers
if [ $USE_NEWLIB -eq 0 ]; then
    cd $LINUX_KERNEL_VERSION
    make ARCH=$LINUX_ARCH INSTALL_HDR_PATH=$INSTALL_PATH/$TARGET headers_install
    cd ..
fi

# Step 3. C/C++ Compilers
mkdir -p build-gcc
cd build-gcc
if [ $USE_NEWLIB -ne 0 ]; then
    NEWLIB_OPTION=--with-newlib
fi
../$GCC_VERSION/configure --prefix=$INSTALL_PATH --target=$TARGET --enable-languages=c --with-system-zlib --enable-tls --enable-languages=c,c++ --disable-libmudflap --disable-libssp --disable-libquadmath --disable-nls --disable-bootstrap --disable-multilib --enable-checking=yes --with-abi=${ABI} --with-arch=${ISA}
make $PARALLEL_MAKE all-gcc
make install-gcc
cd ..

if [ $USE_NEWLIB -ne 0 ]; then
    # Steps 4-6: Newlib
    mkdir -p build-newlib
    cd build-newlib
    ../newlib-master/configure --prefix=$INSTALL_PATH --target=$TARGET $CONFIGURATION_OPTIONS
    make $PARALLEL_MAKE
    make install
    cd ..
else
    # Step 4. Standard C Library Headers and Startup Files
    mkdir -p build-glibc
    cd build-glibc
    ../$GLIBC_VERSION/configure --prefix=$INSTALL_PATH/$TARGET --build=$MACHTYPE --host=$TARGET --target=$TARGET --with-headers=$INSTALL_PATH/$TARGET/include --disable-multilib
    make install-bootstrap-headers=yes install-headers
    make $PARALLEL_MAKE csu/subdir_lib
    install csu/crt1.o csu/crti.o csu/crtn.o $INSTALL_PATH/$TARGET/lib
    $TARGET-gcc -nostdlib -nostartfiles -shared -x c /dev/null -o $INSTALL_PATH/$TARGET/lib/libc.so
    touch $INSTALL_PATH/$TARGET/include/gnu/stubs.h
    cd ..

    # Step 5. Compiler Support Library
    cd build-gcc
    make $PARALLEL_MAKE all-target-libgcc
    make install-target-libgcc
    cd ..

    # Step 6. Standard C Library & the rest of Glibc
    cd build-glibc
    make $PARALLEL_MAKE
    make install
    cd ..
fi

# Step 7. Standard C++ Library & the rest of GCC
cd build-gcc
make $PARALLEL_MAKE all
make install
cd ..

# Step 8. Create some useful links
cd ${INSTALL_PATH}/bin
for i in ${TARGET}*; do
    ln -s $i $(echo $i | sed -e 's/unknown-linux-gnu-//')
done

# Step 9. Qemu
cd $QEMU_VERSION
./configure --prefix=$INSTALL_PATH --interp-prefix=$SYSROOT --target-list=riscv32-linux-user,riscv32-softmmu,riscv64-linux-user,riscv64-softmmu --enable-jemalloc --disable-werror
make $PARALLEL_MAKE
make install
cd ..

trap - EXIT
echo 'Success!'
popd
